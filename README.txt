
The Term Reference URL Widget module adds a new widget to the Term Reference
CCK field type. It auto-populates a term reference field with a value from the
URL, and does not allow this value to be changed once set. This module is largely
based on the Node Reference URL Widget.

Node Reference URL Widget was written by Nate Haug.
Term Reference created by Roy Spliet.

Dependencies
------------
 * Taxonomy

Install
-------
Installing the Term Reference URL Widget is simple:

1) Copy the termreference_url folder to the sites/[site]/modules folder in your
   installation.

2) Enable the module using Administer -> Modules (admin/modules)

3) Add or edit a Term Reference field from admin/structure/types/manage/[type]/fields.
   When configuring the field, use the "Reference from URL" option.

4) Follow on-screen help text to configure your Term Reference URL Widget.

Advanced: Build your own links
------------------------------
Normally you can prepopulate a Term Reference URL widget simply by creating a
link with the following structure:

As HTML:
<a href="/node/add/story/10">Add a story</a>

Or in PHP code:
<a href="<?php print url('node/add/story/' . $term>tid); ?>">Add a story</a>

However if using multiple Term Reference fields, you can populate them by
using a query string instead of embedding the IDs directly in the URL. Assuming
you had two fields, named "field_ref_a" and "field_ref_b", the URL to
prepopulate both of them at the same time would look like this:

In HTML:
<a href="/node/add/story?ref_a=10&ref_b=20">Add a story</a>

Or in PHP code:
<a href="<?php print url('node/add/story', array('query' => array('ref_a' => $tid1, 'ref_b' => $tid2))); ?>">Add a story</a>

Advanced: Support for non-standard URLs
---------------------------------------
By default Term Reference URL Widget will only work with node form paths that
match the standard Drupal install: "node/add/%type", where %type is a node type
like "blog" or "story". If you want to use Term Reference URL Widget on
non-standard URLs, you may do so by informing Term Reference URL Widget of these
special paths.

To do so, add additional paths to your settings.php with the following code:

$conf['termreference_url_paths'] = array(
  'node/add/%type/%tid',
  'node/%/add/%type/%tid',
);

Only two tokens are supported:
%type: The node type that will be created.
%tid: The term ID that will be referenced.

The % wildcard may be used when including other dynamic IDs.

In the above example, Term Reference URL Widget will work at either
"node/add/story/2" or "node/1/add/story/2", where "2" is the term ID being
referenced. Important to note: The first URL will be used in the links that Term
Reference URL Widget provides. If needing advanced configuration of links, look
into the Custom Links module: http://drupal.org/project/custom_links.
